import { Column, Entity, PrimaryColumn, OneToMany } from "typeorm";

@Entity()
export class PostDetails {
  @PrimaryColumn()
  postID: string;

  @Column()
  title: string;

  @Column()
  date: string;

  @Column()
  author:string;

  @Column()
  desc:string;

}