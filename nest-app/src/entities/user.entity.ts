import { Column, Entity, PrimaryColumn, OneToMany } from "typeorm";

@Entity()
export class User {
  @PrimaryColumn()
  username: string;

  @Column()
  password: string;

  @Column({ default: false })
  is_active: boolean;

}