export type Post = {
  postId: number;
  avatarDetails: {
    backgroundColor: string;
    name: string;
  };
  title: string;
  subheader: string;
  image?: string;
  text: string;
};

export type PostsDto = {
  postId: number;
  image: string;
}[];

export const STATUS_OK = 200;
