import { Injectable } from '@nestjs/common';
import { Post, PostsDto } from './types/post.type';
import axios, { AxiosResponse } from 'axios';
import { STATUS_OK } from './types/post.type';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  async getPosts(): Promise<Post[]> {
    const foodPostExample: Post = {
      postId: 1,
      avatarDetails: {
        backgroundColor: 'red',
        name: 'R',
      },
      title: 'Shrimp and Chorizo Paella',
      subheader: 'September 14, 2016',
      text: 'This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
    };

    const beachPostExample: Post = {
      postId: 2,
      avatarDetails: {
        backgroundColor: 'green',
        name: 'Y',
      },
      title: 'Me at the beach :)',
      subheader: 'September 20, 2020',
      text: 'This impressive paella is a perfect party dish and a fun meal to cook together with your guests. Add 1 cup of frozen peas along with the mussels, if you like.',
    };

    const userPosts = [foodPostExample, beachPostExample];

    let flaskPosts: PostsDto = [];
    try {
      flaskPosts = (await axios.get('http://localhost:8000/posts')).data;
    } catch (e) {
      console.error(e);
    }

    const postsWithImages: Post[] = userPosts.map((post: Post) => {
      const image = flaskPosts.find(
        (flaskPost) => flaskPost.postId === post.postId,
      ).image;

      return { ...post, image: image };
    });

    return postsWithImages;
  }
}
